#include <jni.h>
#include <string>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include "RobustMatcher.h"
#include "Model.h"
#include "Utils.h"

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>




extern "C" {

JNIEXPORT jstring JNICALL
Java_com_example_helloopencv_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

jstring
Java_com_example_helloopencv_MainActivity_validate(JNIEnv *env, jobject, jlong addrGray,
                                                   jlong addrRgba) {

    cv::Rect();
    cv::Mat();
    std::string hello2 = "Hello from validate";


    //DEFINICIONES
    std::string featureName = "ORB";
    int numKeyPoints = 2000;
    bool fast_match = true;
    const cv::Mat &frame = <#initializer#>;
    //DEFINICIONES

    RobustMatcher rmatcher;
    //cv::FeatureDetector * detector = new cv::OrbFeatureDetector(numKeyPoints);
    //cv::DescriptorExtractor * extractor = new cv::OrbDescriptorExtractor();
    <cv::FeatureDetector> * detector;
    <cv::FeatureDetector> * descriptor;
    //createFeatures(featureName, numKeyPoints, detector, descriptor);
    createFeatures(featureName, numKeyPoints, reinterpret_cast<cv::Ptr<cv::Feature2D> &>(detector),
                   reinterpret_cast<cv::Ptr<cv::Feature2D> &>(descriptor));
    rmatcher.setFeatureDetector(detector);
    rmatcher.setDescriptorExtractor(descriptor);


    cv::Ptr<cv::flann::IndexParams> indexParams = cv::makePtr<cv::flann::LshIndexParams>(6, 12, 1);
    cv::Ptr<cv::flann::SearchParams> searchParams = cv::makePtr<cv::flann::SearchParams>(50);
    cv::DescriptorMatcher * matcher = new cv::FlannBasedMatcher(indexParams, searchParams);
    rmatcher.setDescriptorMatcher(matcher);


    Model model;
    std::vector<cv::Point3f> list_points3d_model = model.get_points3d();
    cv::Mat descriptors_model = model.get_descriptors();
    std::vector<cv::DMatch> good_matches;       // to obtain the model 3D points  in the scene
    std::vector<cv::KeyPoint> keypoints_scene;
    // to obtain the 2D points of the scene
    if(fast_match)
    {
        rmatcher.fastRobustMatch(frame, good_matches, keypoints_scene, descriptors_model);
    }
    else
    {
        rmatcher.robustMatch(frame, good_matches, keypoints_scene, descriptors_model);
    }




    return env->NewStringUTF(hello2.c_str());
}

}


